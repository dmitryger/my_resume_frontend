import {createRouter, createWebHistory} from "vue-router";
import store from "../store"

const routes = [
    {
        path: "/",
        name: "Home",
        component: () => import("../views/Home"),
    },
    {
        path: "/portfolio",
        name: "Portfolio",
        component: () => import("../views/Portfolio"),
    },
    {
        path: "/portfolio/:id",
        name: "Project",
        component: () => import("../components/portfolio/PortfolioModal")
    },
    {
        path: "/contact",
        name: "Contact",
        component: () => import("../views/Contact")
    },
    {
        path: '/:notFound(.*)',
        component: () => import("../views/Home")
    }
];

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
});

export default router;

router.beforeEach((to, from, next) => {
    store.state.showPreloader = true
    if (store.state.isActiveMenu === true) {
        store.commit('activateMenu')
    }
    if (Boolean(store.state.showPreloader)) {
        store.commit('showToggle')
    }
    if (to.path === '/portfolio') {
        store.state.activePortfolio = true
        store.state.activeHome = false
        store.state.activeContact = false
    } else if (to.path === '/') {
        store.state.activePortfolio = false
        store.state.activeHome = true
        store.state.activeContact = false
    } else if (to.path === '/contact') {
        store.state.activeContact = true
        store.state.activePortfolio = false
        store.state.activeHome = false
    }
    next()
})
