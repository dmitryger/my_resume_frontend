export default {
    namespaced: true,
    state() {
        return {
            projects: "",
            project: ""
        }
    },
    mutations: {
        filterFrontend(state) {
            state.projects.forEach(project => {
                project.show = project.types.find(item => item.name === 'frontend') !== undefined;
            })
        },
        filterAll(state) {
            state.projects.forEach(project => {
                project.show = true
            })
        },
        filterBackend(state) {
            state.projects.forEach(project => {
                project.show = project.types.find(item => item.name === 'backend') !== undefined;
            })
        },
        filterParsing(state) {
            state.projects.forEach(project => {
                project.show = project.types.find(item => item.name === 'parsing') !== undefined;
            })
        },
        allProject(state) {
            if (state.projects === "") {
                fetch('https://dmitry-resume-api.herokuapp.com/api/projects', {
                    method: "GET",
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    mode: "cors"
                }).then(response => response.json())
                    .then(result => {
                        state.projects = result
                    })
            }
        },
        singleProject(state, card_id) {
            fetch(`https://dmitry-resume-api.herokuapp.com/api/projects/${card_id}/`, {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json',
                },
                mode: "cors"
            }).then(response => response.json())
                .then(result => {
                    console.log(result)
                    state.project = result
                })
        }
    },
    actions: {
        mountProjects({commit, dispatch}) {
            commit('allProject')
        },
        mountProject({commit, dispatch}, card_id) {
            commit('singleProject', card_id)
        }
    },
    getters: {
        projects(state) {
            return state.projects
        }
    }
}