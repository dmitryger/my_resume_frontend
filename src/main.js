import {createApp} from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

const app = createApp(App)

app.directive(
    'closeOutside', {
        updated(el, binding) {
            console.log(1)
            el.addEventListener('click', e => e.stopPropagation());
            document.body.addEventListener('click', binding.value);
        }
    }
)

app.use(store).use(router).mount("#app");
