import {createStore} from "vuex";
import user_info from "./state/user_info.module"
import project from "./services/project.module"
import modal from "./state/modal.module"

export default createStore({
    state() {
        return {
            windowWidth: 0,
            isMenu: false,
            isActiveMenu: false,
            showPreloader: true,
            activeHome: true,
            activePortfolio: false,
            activeContact: false,
        }
    },
    mutations: {
        onWindowWidth(state, width) {
            state.windowWidth = width
            state.isMenu = width <= 628;
        },
        activateMenu(state) {
            state.isActiveMenu = state.isActiveMenu === false;

        },
        showToggle(state) {
            setTimeout(() => {
                state.showPreloader = false
            }, 500);
        }
    },
    actions: {},
    modules: {
        user_info,
        project,
        modal
    },
    getters: {}
});
