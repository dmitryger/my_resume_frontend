export default {
    namespaced: true,
    state() {
        return {
            activeModal: false,
            card: ""
        }
    },
    mutations: {
        closeModalWindow(state) {
            state.activeModal = state.activeModal !== true;
        },
        openModalWindow(state, card) {
            state.activeModal = true
            state.card = card
        },

    },
    actions: {},
    getters: {
        activeModal(state) {
            return state.activeModal
        },
        card(state) {
            return state.card
        }
    }
}